/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.inheritance;

/**
 *
 * @author HP
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang" , "Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Object: " + (zom instanceof Object));
        System.out.println("Animal is dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        
        Animal ani1  = null;
        Animal ani2  = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: zom is Duck " +(ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero, zom};
        for(int i=0; i< animals.length; i++) {
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck) {
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
