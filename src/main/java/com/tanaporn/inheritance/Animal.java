/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.inheritance;

/**
 *
 * @author HP
 */
public class Animal {
    protected String name;
    protected int numberOfLegs = 0;
    protected String color;
    
    public Animal(String name, String color, int numberOfLegs) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numberOfLegs = numberOfLegs;
    }
    
    public void walk() {
        System.out.println("Animal walk");
    }
    
    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name: " + this.name + " color: " + this.color + " numberOfLegs:" + this.numberOfLegs);
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public String getColor() {
        return color;
    }
    
    
    
    
}
